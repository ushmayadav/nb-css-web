function initScroll(){
    var lastScrollTop = 0;
    var elem = $('.offer-section_container');
    var elemwidth = elem.width() +"px";
    elem.css("width", elemwidth);
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
    var elemheight = elem.height();
    $(window).scroll(function () {
    if(!isMobile){
    var scroll_top = $(window).scrollTop();
    var elemScrollLimit = $('footer').offset().top - scroll_top - elemheight;
    var windowScrollLimit = $('footer').offset().top - scroll_top - $(window).height();
    var img_height=$('.ad_banner1 img').height()||0;
        if (elemScrollLimit >= 0){
            // Stick on scroll up to footer and header with these condition
                        if (scroll_top > (95+img_height) && scroll_top > lastScrollTop) {	   	
    elem.css({
    "position": "fixed",
    "top":"0px"
    });
        } 
            // Stick on footer for move up after reach at footer till header comes
                        else if (scroll_top > (45+img_height) && scroll_top < lastScrollTop) {
        elem.css({
        "position": "fixed",
        "top": "45px",
        "transition": "all 0.2s linear 0s"
        });
            } 
            // Un-sticky when element goes on top
            else {
        elem.css({
        "position": "",
        "top": "",
        "transition": "all 0.2s linear 0s"
        });
            }
   	
        } else {
            // Un-sticky when element goes on top
        scroll_top = 0;
        if (elemScrollLimit < windowScrollLimit && scroll_top > lastScrollTop){
            var scroll_top_value = elemScrollLimit * (-1);
        elem.css({
        "position": "fixed",
        "top": "",
        "bottom": scroll_top_value,
        "transition":""
        });
        }
        else {
            var scroll_top_value = windowScrollLimit * (-1);
        elem.css({
        "position": "fixed",
        "top": "",
        "bottom": scroll_top_value,
        "transition":""
        });
        }
        }
   	   	
        }
        lastScrollTop = scroll_top;
    });
    }